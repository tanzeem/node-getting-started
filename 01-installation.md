
Installation of Nodejs/npm in Debian GNU/Linux
==============================================

@author : Tanzeem MB, @date: 26/06/2019

Download
--------

Go to the link

`https://nodejs.org/en/download/`

Select Linux Binaries (x64) for
installing Latest LTS Version: 10.16.0 (includes npm 6.9.0)

and download 

`https://nodejs.org/dist/v10.16.0/node-v10.16.0-linux-x64.tar.xz`

Extract the archive `node-v10.16.0-linux-x64.tar.xz`

Environment Setup
-----------------

`sudo mkdir -p /usr/local/nodejs`

`sudo mv node-v10.16.0-linux-x64/* /usr/local/nodejs`

Add `/usr/local/nodejs/bin` to the `PATH` environment variable.

Temporary shell session
^^^^^^^^^^^^^^^^^^^^^^^

For temporay shell session the following command would do

`export PATH=$PATH:/usr/local/nodejs/bin`

This sets the environment variable for your current shell session; when you logout or close the terminal window, your changes will be forgotten.

Setting the path for every new shell session
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Open ~/.bashrc 

- Add to the end of the file

 `export PATH=$PATH:/usr/local/nodejs/bin`

- Reboot

- To check if nodejs is installed successfully, type the commands

```

npm -v

node -v

nodejs -v

```

Tested successfully on Deb GNU 9 on 26.06.2019